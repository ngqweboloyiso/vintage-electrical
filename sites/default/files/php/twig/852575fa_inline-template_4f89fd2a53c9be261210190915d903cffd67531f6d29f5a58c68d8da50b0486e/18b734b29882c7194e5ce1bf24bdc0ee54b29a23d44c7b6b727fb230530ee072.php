<?php

/* {# inline_template_start #}<div class="col-md-4 services-blocks">
<div class="services-top-img">{{ field_services_image }}</div>
<h3>{{ title }}</h3>
<div class="services-intro-text">{{ body }}</div>
<p><div class="readmore-btn"><a href="{{ path }}">Read More</a></div></p>
</div> */
class __TwigTemplate_979ca5b50676856f317925beb431314014b036a8bd0ae9ba47553783d90c8906 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"col-md-4 services-blocks\">
<div class=\"services-top-img\">";
        // line 2
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["field_services_image"]) ? $context["field_services_image"] : null), "html", null, true));
        echo "</div>
<h3>";
        // line 3
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
        echo "</h3>
<div class=\"services-intro-text\">";
        // line 4
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["body"]) ? $context["body"] : null), "html", null, true));
        echo "</div>
<p><div class=\"readmore-btn\"><a href=\"";
        // line 5
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["path"]) ? $context["path"] : null), "html", null, true));
        echo "\">Read More</a></div></p>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"col-md-4 services-blocks\">
<div class=\"services-top-img\">{{ field_services_image }}</div>
<h3>{{ title }}</h3>
<div class=\"services-intro-text\">{{ body }}</div>
<p><div class=\"readmore-btn\"><a href=\"{{ path }}\">Read More</a></div></p>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 5,  59 => 4,  55 => 3,  51 => 2,  48 => 1,);
    }
}
/* {# inline_template_start #}<div class="col-md-4 services-blocks">*/
/* <div class="services-top-img">{{ field_services_image }}</div>*/
/* <h3>{{ title }}</h3>*/
/* <div class="services-intro-text">{{ body }}</div>*/
/* <p><div class="readmore-btn"><a href="{{ path }}">Read More</a></div></p>*/
/* </div>*/
