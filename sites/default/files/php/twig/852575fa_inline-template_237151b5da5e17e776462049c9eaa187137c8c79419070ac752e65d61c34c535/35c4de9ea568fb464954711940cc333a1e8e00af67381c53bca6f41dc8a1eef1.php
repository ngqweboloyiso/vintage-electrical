<?php

/* {# inline_template_start #}<div id="text-carousel" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                     <!--image comes here -->
                             {{ field_slider_image }}
                                 <div class="carousel-caption">
                                        <h1> {{ title }} </h1>
                                             <p>{{ field_slider_description }}</p>
                                 </div>
                  </div>
                      <!-- slider title comes here -->
            </div>
</div> */
class __TwigTemplate_33d78dabe1e554c55767948810b789e2a1bd0463ae5288d5869c00551d3a8ade extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div id=\"text-carousel\" class=\"carousel slide\" data-ride=\"carousel\">
    <!-- Wrapper for slides -->
            <div class=\"carousel-inner\">
                <div class=\"item active\">
                     <!--image comes here -->
                             ";
        // line 6
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["field_slider_image"]) ? $context["field_slider_image"] : null), "html", null, true));
        echo "
                                 <div class=\"carousel-caption\">
                                        <h1> ";
        // line 8
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
        echo " </h1>
                                             <p>";
        // line 9
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["field_slider_description"]) ? $context["field_slider_description"] : null), "html", null, true));
        echo "</p>
                                 </div>
                  </div>
                      <!-- slider title comes here -->
            </div>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div id=\"text-carousel\" class=\"carousel slide\" data-ride=\"carousel\">
    <!-- Wrapper for slides -->
            <div class=\"carousel-inner\">
                <div class=\"item active\">
                     <!--image comes here -->
                             {{ field_slider_image }}
                                 <div class=\"carousel-caption\">
                                        <h1> {{ title }} </h1>
                                             <p>{{ field_slider_description }}</p>
                                 </div>
                  </div>
                      <!-- slider title comes here -->
            </div>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 9,  68 => 8,  63 => 6,  56 => 1,);
    }
}
/* {# inline_template_start #}<div id="text-carousel" class="carousel slide" data-ride="carousel">*/
/*     <!-- Wrapper for slides -->*/
/*             <div class="carousel-inner">*/
/*                 <div class="item active">*/
/*                      <!--image comes here -->*/
/*                              {{ field_slider_image }}*/
/*                                  <div class="carousel-caption">*/
/*                                         <h1> {{ title }} </h1>*/
/*                                              <p>{{ field_slider_description }}</p>*/
/*                                  </div>*/
/*                   </div>*/
/*                       <!-- slider title comes here -->*/
/*             </div>*/
/* </div>*/
